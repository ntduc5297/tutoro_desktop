import 'dart:async';

import '/data/network/constants/endpoints.dart';
import '/data/network/dio_client.dart';

class PostApi {
  // dio instance
  final DioClient _dioClient;

  // rest-client instance

  // injecting dio instance
  PostApi(
    this._dioClient,
  );

  /// Returns list of post in response

  /// sample api call with default rest client
//  Future<PostsList> getPosts() {
//
//    return _restClient
//        .get(Endpoints.getPosts)
//        .then((dynamic res) => PostsList.fromJson(res))
//        .catchError((error) => throw NetworkException(message: error));
//  }
}
