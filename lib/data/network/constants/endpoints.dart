import 'package:flutter_config/flutter_config.dart';

class Endpoints {
  Endpoints._();

  static String mapArgs(String endpoint, Map<String, String> args) {
    var result = endpoint;
    args.forEach((key, value) {
      // ignore: prefer_interpolation_to_compose_strings
      result = result.replaceAll(RegExp(r'\{' + key + '}'), value);
    });
    return result;
  }

  // static const String baseUrl = "http://jsonplaceholder.typicode.com";

  // receiveTimeout
  static const int receiveTimeout = 60000;

  // connectTimeout
  static const int connectionTimeout = 60000;
  // base url
  static String baseUrl = FlutterConfig.get('BASE_URL');

  // static const String getPosts = baseUrl + "/posts";
  // base url

  // check version app
  static const String checkUpdateVersion = '/check_version_app';

  // get and update person info
  static const String personalInfo = '/profile';

  // get and update person info
  static const String jobInfo = '/employments';

  // get OTP
  static const String getOTP = '/user/mobile_otp_registration';

  // verify OTP
  static const String verifyOTP = '/user/verify_mobile_otp_registration';

  // verify OTP
  static const String verifyOTPUpdatePhone = '/user/verify_set_phone_number';

  // register a password
  static const String registrationCreatePass = '/user/registration_password';

  // login with password
  static const String login = '/user/login_pass';

  // login with google
  static const String loginWithGoogle = '/user/social_auth/google';

  // login with google
  static const String loginWithApple = '/user/social_auth/apple';

  // login with facebook
  static const String loginWithFacebook = '/user/social_auth/facebook';

  // login with microsoft
  static const String loginWithMicrosoft = '/user/social_auth/microsoft';

  // logout
  static const String logout = '/user/logout';

  // forgot password
  static const String forgotPassword = '/user/forgot_password';

  // set email
  static const String setEmail = '/user/set_email';

  // verify OTP email
  static const String verifyOTPEmail = '/user/verify_otp_email';

  // change pass
  static const String changePwd = '/user/change_password';

  // change language
  static const String changeLanguage = '/user/change_language';

  // get and update person info
  static const String professionalExp = '/professional_experiences';

  //master data
  static const String countries = '/masterdata/countries';
  static const String provinces = '/masterdata/provinces';
  static const String districts = '/masterdata/districts';
  static const String wards = '/masterdata/wards';
  static const String trainingUnits = '/masterdata/training_units';
  static const String academicTitles = '/masterdata/academic_titles';
  static const String termAndRules =
      '/page_cms/terms_and_rules?language_code={languageCode}';
  static const String subjects =
      '/masterdata/subjects?language_code={languageCode}';
  static const String grades =
      '/masterdata/grades?language_code={languageCode}';

  // get contracts
  static const String getContracts = '/contracts';

  // get academic level
  static const String getAcademicLevels = '/academic_levels';

  // add academic level
  static const String addAcademicLevel = '/academic_levels';

  // edit academic level
  static const String editAcademicLevel = '/academic_levels';

  // get certificates
  static const String getCertifications = '/certifications';
  // edit certificates
  static const String editCertifications = '/certifications';
  // add certificates
  static const String addCertifications = '/certifications';

  // add certificates
  static const String imagesSelf = '/personal_images';

  // get class available classes
  static const String availableClasses = '/classes/available_classes';

  // get class teach in today
  static const String countClassToday = '/class_sessions/count_session';

  // register class
  static const String registerClass = '/classes/register_class';

  // cancel register class
  static const String cancelRegisterClass = '/classes/cancel_register_class';

  // fetch class sessions
  static const String classSessions = '/class_sessions';

  // fetch class teaching
  static const String classTeaching = '/classes/registered_classes';

  // fetch student in class
  static const String studentsInClass =
      '/classes/registered_classes/{idClass}/students';

  static const String loginAsStudent = '/qr_login/login_as_student/';

  // cancel teaching in class
  static const String cancelTeaching =
      '/classes/registered_classes/{idClass}/stop';

  // cancel teaching in class
  static const String cancelTeachingSession =
      '/class_sessions/{session_id}/request_off';

  //checkin lesson
  static const String checkInClass = '/class_sessions/{session_id}/checkin';

  //report lesson plan
  static const String reportLessonPlan =
      '/lesson_plans/{lesson_plan_id}/report';

  //send lesson to mail
  static const String sendLessonPlanToEmail =
      '/lesson_plans/{lesson_plan_id}/send_email';

  //get detail lesson
  static const String detailSession = '/class_sessions/{session_id}';

  //get detail lesson
  static const String checkinStatus = '/class_sessions/{session_id}/checkin';

  // report in lesson
  static const String reportLesson =
      '/class_sessions/{session_id}/statistic_report';

  // report in class
  static const String reportClass =
      '/classes/registered_classes/{idClass}/statistic_report';

  // fetch list attendances
  static const String getAttendances =
      '/class_sessions/{session_id}/attendances?language_code={language_code}';

  // fetch list attendances
  static const String doAttention =
      '/class_sessions/{session_id}/attendances/{student_id}';

  // fetch list attendances
  static const String getLessonProducts =
      '/products/class_sessions/{session_id}/products';

  // fetch detail lesson
  static const String getDetailProduct =
      '/products/class_sessions/{session_id}/product?student_id={student_id}';

  // create lesson product
  static const String createLessonProduct =
      '/products/class_sessions/{session_id}/products';

  // create lesson product
  static const String updateLessonProduct =
      '/products/class_sessions/{session_id}/products';

  // create lesson product
  static const String deleteLessonProduct =
      '/products/class_sessions/{session_id}/product';

  static const String getLessonPlans =
      '/class_sessions/{session_id}/lesson_plans';

  // fetch list image and video in lesson
  static const String getListImagesAndVideos =
      '/class_sessions/{session_id}/images_and_videos';

  static const String removeImagesOrVideos =
      '/class_sessions/{session_id}/images_and_videos';

  static const String uploadLessonImageOrVideo =
      '/class_sessions/{session_id}/images_and_videos';

  // get and update person info
  static const String otpUpdatePhone = '/user/set_phone_number';

  // fetch list image and video in lesson
  static const String fetchCancelClassStatus =
      '/classes/registered_classes/{class_id}/stop_info';

  static const String fetchReportStudentInLesson =
      '/class_sessions/{class_id}/students/{student_id}/detail';

  static const String requestOffReason =
      '/class_sessions/list_request_off_reasons';

  static const String cancelUpdateProfile = '/profile/cancel_profile_changes';

  static const String submitProfile = '/profile/submit_profile_changes';

  //get reason off in lesson
  static const String reasonOff = '/class_sessions/list_request_off_reasons';

  static const String getClassSessionEvaluations =
      '/class_sessions/{session_id}/evaluations';
  static const String sendStudentEvaluationForClassSession =
      '/class_sessions/{session_id}/evaluations/{student_id}';
  static const String getEvaluationTemplates =
      '/class_sessions/{session_id}/evaluation_templates';
  static const String getEvaluationInfo =
      '/class_sessions/{session_id}/evaluation_information/{student_id}';

  // refer and earn
  static const String recommendationList = '/user/recommendations';

  // notifications
  static const String getNotifications = '/notifications';
  static const String deleteNotification = '/notifications/{notification_id}';
  static const String markNotificationsAsRead = '/notifications/mark_as_read';

  // check version
  static const String checkVersion = '/user/check_version/{device_version}';

  // borrow material
  static const String borrowMaterialRequest =
      '/learning_material/{session_id}/detail';
  static const String sendborrowMaterialRequest =
      '/learning_material/{session_id}/request_borrow';

  static const String listMaterial =
      '/learning_material/list_materials_categories';
}
