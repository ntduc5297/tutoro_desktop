import 'package:another_flushbar/flushbar_helper.dart';
import 'package:tutorO/constants/assets.dart';
import 'package:tutorO/constants/colors.dart';
import 'package:tutorO/data/sharedpref/constants/preferences.dart';
import 'package:tutorO/utils/routes/routes.dart';
import 'package:tutorO/stores/form/form_store.dart';
import 'package:tutorO/stores/theme/theme_store.dart';
import 'package:tutorO/utils/device/device_utils.dart';
import 'package:tutorO/utils/locale/app_localization.dart';
import 'package:tutorO/widgets/app_icon_widget.dart';
import 'package:tutorO/widgets/empty_app_bar_widget.dart';
import 'package:tutorO/widgets/number_field.dart';
import 'package:tutorO/widgets/password_field.dart';
import 'package:tutorO/widgets/progress_indicator_widget.dart';
import 'package:tutorO/widgets/rounded_button_widget.dart';

import 'package:tutorO/widgets/textfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //text controllers:-----------------------------------------------------------
  TextEditingController _userEmailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  //stores:---------------------------------------------------------------------
  late ThemeStore _themeStore;

  //focus node:-----------------------------------------------------------------
  late FocusNode _passwordFocusNode;

  //stores:---------------------------------------------------------------------
  final store = FormStore();

  @override
  void initState() {
    store.setupValidations();
    super.initState();
    store.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _themeStore = Provider.of<ThemeStore>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: true,
      appBar: EmptyAppBar(),
      body: Observer(builder: (context) {
        return _buildBody();
      }),
    );
  }

  // body methods:--------------------------------------------------------------
  Widget _buildBody() {
    return Material(
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Assets.backgroundLoginImg),
                fit: BoxFit.cover)),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: AppColors.mainColor.withOpacity(0.6),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 200),
              child: Center(child: _buildLoginContent()),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginContent() {
    return SingleChildScrollView(
      child: Container(
        height: 600,
        width: 400,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: AppColors.mainShadowColor,
                  offset: Offset(1, 1),
                  blurRadius: 10)
            ],
            border: Border.all(
                color: AppColors.mainBorderColor,
                style: BorderStyle.solid,
                width: 1),
            color: Colors.white.withOpacity(0.9),
            borderRadius: BorderRadius.circular(15)),
        padding: const EdgeInsets.symmetric(horizontal: 35.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: AppIconWidget(image: Assets.logo)),
            Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  AppLocalizations.of(context).translate('login'),
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                )),
            SizedBox(height: 24.0),
            _buildUserIdField(),
            Container(
                width: double.infinity,
                child: Container(
                  padding: EdgeInsets.only(top: 5),
                  child: Text(
                    '${store.error.number == null ? '' : store.error.number}',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.red),
                  ),
                )),
            SizedBox(height: 7),
            _buildPasswordField(),
            Container(
                width: double.infinity,
                child: Container(
                  padding: EdgeInsets.only(top: 5),
                  child: store.error.password == null
                      ? Text('')
                      : Text(
                          '${store.error.password}',
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.red),
                        ),
                )),
            _buildForgotPasswordButton(),
            SizedBox(
              height: 15,
            ),
            _buildSignInButton()
          ],
        ),
      ),
    );
  }

  Widget _buildUserIdField() {
    return NumberField(
      onChanged: ((number) => {
            store.setNumber(number),
            // if (email == "") {store.error.email = null}
          }),
    );
  }

  Widget _buildPasswordField() {
    return PasswordField(
      onChanged: ((password) => {store.setPassword(password)}),
    );
  }

  Widget _buildForgotPasswordButton() {
    return Align(
        alignment: FractionalOffset.centerRight,
        child: TextButton(
          style: ButtonStyle(
              overlayColor: MaterialStateColor.resolveWith(
                  (states) => AppColors.mainColor.withOpacity(0.05))),
          onPressed: () {
            Navigator.of(context).pushNamed(Routes.forgotpassword);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            child: Text(
              AppLocalizations.of(context)
                  .translate('login_btn_forgot_password'),
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium
                  ?.copyWith(color: AppColors.mainTextColor),
            ),
          ),
        ));
  }

  Widget _buildSignInButton() {
    return RoundedButtonWidget(
      isEnable: store.canLogin,
      buttonColorHover: AppColors.mainColor.withOpacity(0.7),
      buttonText: AppLocalizations.of(context).translate('login'),
      buttonColor: AppColors.mainColor,
      textColor: Colors.white,
      onPressed: () async {
        Navigator.of(context).pushNamed(Routes.home);
        // if (_store.canLogin) {
        //   DeviceUtils.hideKeyboard(context);
        //   _store.login();
        // } else {
        //   _showErrorMessage('Please fill in all fields');
        // }
      },
    );
  }

  Widget navigate(BuildContext context) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(Preferences.is_logged_in, true);
    });

    Future.delayed(Duration(milliseconds: 0), () {
      Navigator.of(context).pushNamedAndRemoveUntil(
          Routes.home, (Route<dynamic> route) => false);
    });

    return Container();
  }

  // General Methods:-----------------------------------------------------------
  _showErrorMessage(String message) {
    if (message.isNotEmpty) {
      Future.delayed(Duration(milliseconds: 0), () {
        if (message.isNotEmpty) {
          FlushbarHelper.createError(
            message: message,
            title: AppLocalizations.of(context).translate('home_tv_error'),
            duration: Duration(seconds: 3),
          )..show(context);
        }
      });
    }

    return SizedBox.shrink();
  }

  // dispose:-------------------------------------------------------------------
  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    _userEmailController.dispose();
    _passwordController.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }
}
