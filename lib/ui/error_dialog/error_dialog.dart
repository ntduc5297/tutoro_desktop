import 'package:flutter/material.dart';
import 'package:flutter_screen_scaling/flutter_screen_scaling.dart';

import '../../constants/assets.dart';
import '../../constants/colors.dart';
import '../../utils/locale/app_localization.dart';
// import '../auth/common/confirm_button/confirm_button.dart';

class ErrorDialog extends StatelessWidget {
  final BuildContext dialogContext;
  final Function? btnConfirmCallback;
  final String? message;

  ErrorDialog(
      {required this.dialogContext, this.message, this.btnConfirmCallback});

  @override
  Widget build(BuildContext context) {
    final _appLocalizations = AppLocalizations.of(context);
    return Dialog(
      backgroundColor: Colors.white.withOpacity(0.3),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            alignment: Alignment.center,
            width: ScreenScale.convertWidth(345),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Color(0xffe3e8f9)),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      blurRadius: ScreenScale.convertHeight(20),
                      offset: Offset(0, ScreenScale.convertHeight(10)),
                      color: Color(0xffb1bcc7).withOpacity(0.25))
                ]),
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  constraints: BoxConstraints.tight(Size(345, 419)),
                  padding: EdgeInsets.only(top: ScreenScale.convertHeight(49)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            bottom: ScreenScale.convertHeight(27)),
                        // child: Image.asset(
                        //   Assets.errorDialogImg,
                        //   width: ScreenScale.convertWidth(142),
                        //   height: ScreenScale.convertWidth(132),
                        // ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            bottom: ScreenScale.convertWidth(15)),
                        child: Text(
                          _appLocalizations.translate('error_dialog_title'),
                          style: TextStyle(
                            fontSize: ScreenScale.convertFontSize(22),
                            fontWeight: FontWeight.w700,
                            // color: AppColors.mainTextColor
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            bottom: ScreenScale.convertWidth(33)),
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenScale.convertWidth(53)),
                        child: Text(
                          message ??
                              _appLocalizations.translate('error_dialog_msg'),
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              height: 1.25,
                              fontSize: ScreenScale.convertFontSize(16),
                              color: Color(0xff4b5574)),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            bottom: ScreenScale.convertHeight(16)),
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenScale.convertWidth(33)),
                        // child: ConfirmButton(
                        //   title: 'OK',
                        //   callback: () {
                        //     Navigator.of(dialogContext).pop();
                        //     if (btnConfirmCallback != null) {
                        //       btnConfirmCallback!();
                        //     }
                        //   },
                        // ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.of(dialogContext).pop();
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenScale.convertWidth(12),
                          vertical: ScreenScale.convertHeight(17)),
                      // child: Image.asset(
                      //   Assets.xIcon,
                      //   width: ScreenScale.convertWidth(20),
                      //   height: ScreenScale.convertWidth(20),
                      // ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
