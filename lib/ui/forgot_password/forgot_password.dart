import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/svg.dart';
import '/constants/assets.dart';
import '/constants/colors.dart';
import '/stores/form/form_store.dart';
import '/utils/device/device_utils.dart';
import '/utils/locale/app_localization.dart';
import '/widgets/rounded_button_widget.dart';
import '/widgets/textfield_widget.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  TextEditingController _userEmailController = TextEditingController();
  final _store = FormStore();
  late FocusNode _passwordFocusNode;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.symmetric(
          horizontal: 100,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildLeftSide(),
            _buildRightSide(),
          ],
        ),
      ),
    );
  }

  Widget _buildLeftSide() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      height: 400,
      child: SvgPicture.asset(Assets.forgotPass, fit: BoxFit.cover,
          placeholderBuilder: (context) {
        return SkeletonListView();
      }),
    );
  }

  Widget _buildRightSide() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 100),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              color: AppColors.mainBorderColor,
              style: BorderStyle.solid,
              width: 1),
          boxShadow: [
            BoxShadow(
                color: AppColors.mainShadowColor,
                offset: Offset(1, 1),
                blurRadius: 10)
          ]),
      width: 400,
      height: 500,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context)
                .translate('please_fill_email_to_reset_password'),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 25),
            width: 300,
            child: _buildUserIdField(),
          ),
          _buildConfirmButton(),
          SizedBox(height: 10),
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(AppLocalizations.of(context).translate('back'),
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: AppColors.mainTextColor)),
              ))
        ],
      ),
    );
  }

  Widget _buildUserIdField() {
    return Observer(
      builder: (context) {
        return TextFieldWidget(
          hint: AppLocalizations.of(context).translate('login_et_user_email'),
          inputType: TextInputType.emailAddress,
          icon: Assets.singleUserGrayIcon,
          textController: _userEmailController,
          inputAction: TextInputAction.next,
          autoFocus: false,
          onChanged: (value) {},
          maxLength: 50,
          onFieldSubmitted: (value) {
            FocusScope.of(context).requestFocus(_passwordFocusNode);
          },
        );
      },
    );
  }

  Widget _buildConfirmButton() {
    return RoundedButtonWidget(
      buttonColorHover: AppColors.mainColor.withOpacity(0.7),
      buttonText: AppLocalizations.of(context).translate('confirm'),
      buttonColor: AppColors.mainColor,
      textColor: Colors.white,
      onPressed: () {},
      isEnable: true,
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    _userEmailController.dispose();
    super.dispose();
  }
}
