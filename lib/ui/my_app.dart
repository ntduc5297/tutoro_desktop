import '/constants/app_theme.dart';
import '/constants/colors.dart';
import '/constants/font_family.dart';
import '/constants/strings.dart';
import '../data/repository/repository.dart';
import '/stores/language/language_store.dart';

import '/stores/theme/theme_store.dart';
import '/stores/user/user_store.dart';
import '/ui/home/home.dart';
import '/ui/login/login.dart';
import '/utils/locale/app_localization.dart';
import '/utils/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // Create your store as a final variable in a base Widget. This works better
  // with Hot Reload than creating it directly in the `build` function.
  @override
  Widget build(BuildContext context) {
    return Observer(
      name: 'global-observer',
      builder: (context) {
        return MaterialApp(
          theme: ThemeData(
              fontFamily: FontFamily.quickSand,
              hoverColor: AppColors.mainColor,
              primaryColor: AppColors.mainColor,
              textTheme: TextTheme(bodyMedium: TextStyle(fontSize: 16))),
          debugShowCheckedModeBanner: false,
          title: Strings.appName,
          routes: Routes.routes,
          // locale: Locale(_languageStore.locale),
          // supportedLocales: _languageStore.supportedLanguages
          //     .map((language) => Locale(language.locale!, language.code))
          //     .toList(),
          localizationsDelegates: [
            // A class which loads the translations from JSON files
            AppLocalizations.delegate,
            // Built-in localization of basic text for Material widgets
            GlobalMaterialLocalizations.delegate,
            // Built-in localization for text direction LTR/RTL
            GlobalWidgetsLocalizations.delegate,
            // Built-in localization of basic text for Cupertino widgets
            GlobalCupertinoLocalizations.delegate,
          ],
          // home: _userStore.isLoggedIn ? HomeScreen() : LoginScreen(),
          home: LoginScreen(),
        );
      },
    );
  }
}
