import 'package:flutter/material.dart';
import 'package:tutorO/constants/colors.dart';

class RoundedButtonWidget extends StatefulWidget {
  final String buttonText;
  final Color buttonColor;
  final Color buttonColorHover;
  final Color textColor;
  final VoidCallback onPressed;
  final bool? isEnable;

  const RoundedButtonWidget({
    Key? key,
    this.isEnable = false,
    required this.buttonText,
    required this.buttonColor,
    this.textColor = Colors.white,
    required this.onPressed,
    required this.buttonColorHover,
  }) : super(key: key);

  @override
  State<RoundedButtonWidget> createState() => _RoundedButtonWidgetState();
}

class _RoundedButtonWidgetState extends State<RoundedButtonWidget> {
  bool isHover = true;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) {
        setState(() {
          isHover = false;
        });
      },
      onExit: (event) {
        setState(() {
          isHover = true;
        });
      },
      child: Container(
        child: InkWell(
          onTap: widget.onPressed,
          child: AnimatedContainer(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(99),
              color: widget.isEnable == true
                  ? (isHover == true
                      ? widget.buttonColor
                      : widget.buttonColorHover)
                  : AppColors.mainShadowColor,
            ),
            duration: Duration(milliseconds: 300),
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 60),
            child: Text(
              widget.buttonText,
              style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ),
    );
  }
}
