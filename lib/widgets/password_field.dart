import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tutorO/constants/assets.dart';
import 'package:tutorO/constants/colors.dart';
import 'package:tutorO/utils/locale/app_localization.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({Key? key, this.onChanged, this.errorText})
      : super(key: key);
  final ValueChanged<String>? onChanged;
  final String? errorText;
  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _iSObsecure = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: AppColors.mainShadowColor,
                blurRadius: 7,
                offset: Offset(0, 1))
          ],
          color: Colors.white,
          border: Border.all(
            color: Color(0xffCBD9E6),
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(4)),
      child: Row(
        children: [
          Expanded(
              flex: 1, child: Center(child: SvgPicture.asset(Assets.lockIcon))),
          Expanded(
            flex: 9,
            child: TextField(
              onChanged: widget.onChanged,
              cursorColor: Color(0xff7B57FB),
              obscureText: _iSObsecure,
              decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)
                      .translate('login_et_user_password'),
                  errorText: widget.errorText,
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _iSObsecure = !_iSObsecure;
                        });
                      },
                      // icon: Icon(
                      //   _iSObsecure ? obsecureicon.icon1 : Icons.visibility_off,
                      // )
                      icon: _iSObsecure
                          ? obsecureicon.icon1
                          : obsecureicon.icon2)),
            ),
          ),
        ],
      ),
    );
  }
}

class obsecureicon {
  static final icon1 = Icon(Icons.visibility, color: Color(0xff9EA9C9));
  static final icon2 = Icon(Icons.visibility_off, color: Color(0xff9EA9C9));
}
