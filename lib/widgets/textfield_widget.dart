import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '/constants/colors.dart';

class TextFieldWidget extends StatelessWidget {
  final String? icon;
  final String? hint;

  final bool isObscure;
  final bool isIcon;
  final TextInputType? inputType;
  final TextEditingController textController;
  final EdgeInsets padding;
  final Color hintColor;
  final Color iconColor;
  final FocusNode? focusNode;
  final ValueChanged? onFieldSubmitted;
  final ValueChanged? onChanged;
  final bool autoFocus;
  final TextInputAction? inputAction;
  final int? maxLength;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            border: Border.all(
                color: AppColors.secondTextColor,
                width: 1,
                style: BorderStyle.solid)),
        child: TextFormField(
          controller: textController,
          focusNode: focusNode,
          onFieldSubmitted: onFieldSubmitted,
          onChanged: onChanged,
          autofocus: autoFocus,
          textInputAction: inputAction,
          obscureText: this.isObscure,
          maxLength: this.maxLength ?? 25,
          keyboardType: this.inputType,
          style: Theme.of(context).textTheme.bodyLarge,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: this.hint,
              hintStyle: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .copyWith(color: hintColor),
              counterText: '',
              icon: this.isIcon && icon != ''
                  ? Container(
                      height: 25,
                      width: 25,
                      child: this.icon != null && this.icon!.contains('svg')
                          ? SvgPicture.asset(this.icon!)
                          : Image.asset(this.icon!))
                  : null),
        ),
      ),
    );
  }

  const TextFieldWidget({
    Key? key,
    this.icon,
    required this.textController,
    this.inputType,
    this.hint,
    this.isObscure = false,
    this.isIcon = true,
    this.padding = const EdgeInsets.all(0),
    this.hintColor = Colors.grey,
    this.iconColor = Colors.grey,
    this.focusNode,
    this.onFieldSubmitted,
    this.onChanged,
    this.autoFocus = false,
    this.inputAction,
    this.maxLength,
  }) : super(key: key);
}
