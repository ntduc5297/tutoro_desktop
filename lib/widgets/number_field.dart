import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tutorO/constants/assets.dart';
import 'package:tutorO/constants/colors.dart';
import 'package:tutorO/utils/locale/app_localization.dart';

class NumberField extends StatefulWidget {
  const NumberField({Key? key, this.errorText, this.onChanged})
      : super(key: key);
  final String? errorText;
  final ValueChanged<String>? onChanged;
  @override
  State<NumberField> createState() => _NumberFieldState();
}

class _NumberFieldState extends State<NumberField> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Color(0xffCBD9E6),
              width: 1,
              style: BorderStyle.solid,
            ),
            boxShadow: [
              BoxShadow(
                  color: AppColors.mainShadowColor,
                  blurRadius: 7,
                  offset: Offset(0, 1))
            ],
            borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            Expanded(
                flex: 1,
                child: Center(
                  child: Image.asset(
                    Assets.singleUserGrayIcon,
                    height: 25,
                  ),
                )),
            Expanded(
              flex: 9,
              child: TextField(
                  maxLength: 10,
                  onChanged: widget.onChanged,
                  cursorColor: AppColors.mainColor,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: InputDecoration(
                      counterText: '',
                      contentPadding: EdgeInsets.only(right: 10),
                      border: InputBorder.none,
                      hintText: AppLocalizations.of(context)
                          .translate('login_user_number'),
                      hintStyle: TextStyle())),
            ),
          ],
        ));
  }
}
