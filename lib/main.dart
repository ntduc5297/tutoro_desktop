import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:another_flushbar/flushbar_helper.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';
import 'constants/app_theme.dart';
import 'constants/colors.dart';
import 'constants/strings.dart';
import 'di/components/app_component.dart';
import 'di/module/local_module.dart';
import '/di/module/network_module.dart';
import 'di/module/preference_module.dart';
import 'stores/theme/theme_store.dart';
import 'stores/language/language_store.dart';
import 'utils/locale/app_localization.dart';
import 'ui/splash/splash.dart';
import '../ui/login/login.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_screen_scaling/flutter_screen_scaling.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:navigation_history_observer/navigation_history_observer.dart';
import 'package:sendbird_sdk/sendbird_sdk.dart';
import 'package:uni_links/uni_links.dart';
import 'package:lehttp_overrides/lehttp_overrides.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:desktop_window/desktop_window.dart';

// global instance for app component
late AppComponent appComponent;
late GlobalKey<NavigatorState> globalNavigatorKey;

// Sets a platform override for desktop to avoid exceptions. See
// https://flutter.dev/desktop#target-platform-override for more info.
void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background,
  // such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");

  print("Message data: ${message.data}");

  if (message.notification != null) {
    print("Message also contianed a notification: ${message.notification}");
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Future testWindowFunctions() async {
    await DesktopWindow.setWindowSize(Size(1280, 760));
    await DesktopWindow.setMinWindowSize(Size(1280, 760));
    await DesktopWindow.setMaxWindowSize(Size(1600, 900));
  }

  await testWindowFunctions();
  _enablePlatformOverrideForDesktop();
  // FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) async {
    appComponent = await AppComponent.create(
      NetworkModule(),
      LocalModule(),
      PreferenceModule(),
    );
    // Spy MobX in debug mode
    // if (kDebugMode) {
    //   mainContext.spy(print);
    // }
    WidgetsFlutterBinding.ensureInitialized();
    // initializeDateFormatting().then((_) => runApp(appComponent.app));
    // await FlutterConfig.loadEnvVariables();
    // await Firebase.initializeApp();
    // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    // if (Platform.isAndroid) {
    //   HttpOverrides.global = LEHttpOverrides();
    // }
    runApp(appComponent.app);
  });
  if (kDebugMode) {
    // Force disable Crashlytics collection while doing every day development.
    // Temporarily toggle this to true if you want to test crash reporting in your app.
    // print(
    //     "Force disable Crashlytics collection while doing every day development.");
    // await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    // await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
  } else {
    // Handle Crashlytics enabled status when not in Debug,
    // e.g. allow your users to opt-in to crash reporting.
    // await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    // SystemChrome.setEnabledSystemUIOverlays([]);
  }
}

class MyStatefulApp extends StatefulWidget {
  // final ThemeStore _themeStore = ThemeStore(appComponent.getRepository());
  // final LanguageStore _languageStore =
  //     LanguageStore(appComponent.getRepository());
  // final AppStatusStore _appStatusStore =
  //     AppStatusStore(repository: appComponent.getRepository());
  // final AuthenticationStore _authenticationStore =
  //     AuthenticationStore(appComponent.getAuthenticationRepository());
  // final PersonalInfoStore _personalInfoStore = PersonalInfoStore(
  //   workExpRepository: appComponent.getWorkExpRepository(),
  //   profileRepository: appComponent.getProfileRepository(),
  //   academicLevelRepository: appComponent.getAcademicLevelRepository(),
  //   certificationRepository: appComponent.getCertificationRepository(),
  // );
  // final ClassScheduleStore _classScheduleStore = ClassScheduleStore(
  //   classScheduleRepository: appComponent.getClassRepository(),
  // );
  // final TeachingManageStore _teachingManageStore =
  //     TeachingManageStore(classRepository: appComponent.getClassRepository());

  // final ClassDetailStore _classDetailStore =
  //     ClassDetailStore(classRepository: appComponent.getClassRepository());

  // final LessonStore _lessonStore =
  //     LessonStore(classRepository: appComponent.getClassRepository());
  // final LessonProductsStore _lessonProductsStore =
  //     LessonProductsStore(classRepository: appComponent.getClassRepository());
  // final MasterDataStore _masterDataStore =
  //     MasterDataStore(appComponent.getMasterDataRepository());

  // final StateAppStore _stateApp = StateAppStore();

  @override
  _MyStatefulAppState createState() => _MyStatefulAppState();
}

class _MyStatefulAppState extends State<MyStatefulApp> {
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey<NavigatorState>();
  late ReactionDisposer _disposer;
  final sendbird = SendbirdSdk(appId: '089816A9-C751-4294-9644-5B5F80727165');
  // Universal link handle
  StreamSubscription? _sub;

  @override
  void initState() {
    super.initState();

    globalNavigatorKey = _navigatorKey;
    // _disposer = reaction(
    //     (_) => widget._appStatusStore.connectivityStream!.value, (result) {
    //   var context = _navigatorKey.currentState!.overlay!.context;
    //   if (result == ConnectivityResult.none) {
    //     _showErrorMessage(
    //         AppLocalizations.of(context).translate('common_no_network'),
    //         context);
    //   }
    // }, delay: 4000);
    ScreenScale.init(allowSubpixel: true, allowFontScaling: true);
    initPlatformState();
    ScreenScaleProperties(width: 414, height: 896, allowFontScaling: false);

    // _handleIncomingLinks();
  }

  void initPlatformState() async {
    try {
      // ignore: avoid_catches_without_on_clauses
    } catch (e) {
      print("Error found!!!! $e");
      rethrow;
    }
    if (!mounted) return;
  }

  @override
  void didUpdateWidget(covariant MyStatefulApp oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _disposer();
    _sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      name: 'global-observer',
      builder: (context) {
        return MaterialApp(home: ExampleWidget());
      },
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return MultiProvider(
  //     providers: [
  // Provider<ThemeStore>(create: (_) => widget._themeStore),
  // Provider<LanguageStore>(create: (_) => widget._languageStore),
  // Provider<AppStatusStore>(create: (_) => widget._appStatusStore),
  // Provider<AuthenticationStore>(
  //     create: (_) => widget._authenticationStore),
  // Provider<MasterDataStore>(create: (_) => widget._masterDataStore),
  // Provider<PersonalInfoStore>(create: (_) => widget._personalInfoStore),
  // Provider<ClassScheduleStore>(create: (_) => widget._classScheduleStore),
  // Provider<LessonStore>(create: (_) => widget._lessonStore),
  // Provider<TeachingManageStore>(
  //     create: (_) => widget._teachingManageStore),
  // Provider<ClassDetailStore>(create: (_) => widget._classDetailStore),
  // Provider<LessonProductsStore>(
  //     create: (_) => widget._lessonProductsStore),
  // Provider<StateAppStore>(create: (_) => widget._stateApp)

  // child: Observer(
  //   name: 'global-observer',
  //   builder: (context) {
  //     return GlobalLoaderOverlay(
  //       useDefaultLoading: false,
  //       overlayColor: Colors.black,
  //       overlayWidget: Center(
  //         child: SpinKitPouringHourglass(
  //           color: AppColors.mainColor,
  //           size: 50,
  //         ),
  //       ),
  //       child: RefreshConfiguration(
  //         footerTriggerDistance: 200,
  //         maxUnderScrollExtent: 100,
  //         child: MaterialApp(
  //           navigatorKey: _navigatorKey,
  //           debugShowCheckedModeBanner: false,
  //           title: Strings.appName,
  //           theme: themeData,
  //           routes: Routes.routes,
  //           onGenerateRoute: Routes.onGenerateRoute,
  //           locale: Locale(widget._languageStore.locale),
  //           supportedLocales: widget._languageStore.supportedLanguages
  //               .map((language) => Locale(language.locale, language.code))
  //               .toList(),
  //           localizationsDelegates: [
  //             // A class which loads the translations from JSON files
  //             AppLocalizations.delegate,
  //             // Built-in localization of basic text for Material widgets
  //             GlobalMaterialLocalizations.delegate,
  //             // Built-in localization for text direction LTR/RTL
  //             GlobalWidgetsLocalizations.delegate,
  //             // Built-in localization of basic text for Cupertino widgets
  //             GlobalCupertinoLocalizations.delegate,
  //           ],
  //           builder: (context, child) => Stack(
  //             children: [
  //               child!,
  //               DropdownNotification(
  //                 delayDismiss: 5000,
  //               )
  //             ],
  //           ),
  //           home: SplashScreen(),
  //           navigatorObservers: [NavigationHistoryObserver()],
  //         ),
  //       ),
  //     );
  //   },
  // ),
  //   );
  // }

  _showErrorMessage(String? message, BuildContext context) {
    Future.delayed(Duration(milliseconds: 0), () {
      if (message != null && message.isNotEmpty) {
        FlushbarHelper.createError(
          message: message,
          title: AppLocalizations.of(context).translate('common_error'),
          duration: Duration(seconds: 3),
        ).show(context);
      }
    });
  }

  /// Handle incoming links - the ones that the app will receive from the OS
  /// while already started.
  // void _handleIncomingLinks() {
  //   if (!kIsWeb) {
  //     // It will handle app links while the app is already started - be it in
  //     // the foreground or in the background.
  //     _sub = uriLinkStream.listen((uri) {
  //       if (!mounted) return;
  //       print('got uri: $uri');
  //       _handleRefCodeUniversalLink(uri);
  //     }, onError: (err) {
  //       if (!mounted) return;
  //       print('got err: $err');
  //     });
  //   }
  // }

  // void _handleRefCodeUniversalLink(Uri? uri) {
  //   if (widget._authenticationStore.isLoggedIn == false &&
  //       uri != null &&
  //       uri.host != "authorize") {
  //     var _currentRouteName = NavigationHistoryObserver().top?.settings.name;
  //     if (_currentRouteName != Routes.register) {
  //       if (_navigatorKey.currentContext != null) {
  //         var refCode = uri.queryParameters.containsKey('ref')
  //             ? uri.queryParameters['ref']
  //             : null;
  //         Navigator.of(_navigatorKey.currentContext!).pushNamed(Routes.register,
  //             arguments: RegisterScreenArgument(refCode: refCode));
  //       }
  //     }
  //   }
  // }
}

class ExampleWidget extends StatefulWidget {
  const ExampleWidget({Key? key}) : super(key: key);

  @override
  State<ExampleWidget> createState() => _ExampleWidgetState();
}

class _ExampleWidgetState extends State<ExampleWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Text("Welcome!")),
    );
  }
}
