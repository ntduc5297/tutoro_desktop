class Dimens {
  Dimens._();

  //for all screens
  static const double horizontalPadding = 20.0;
  static const double verticalPadding = 20.0;

  static const double rootHorizontalPadding = 25.0;
  static const double rootVerticalPadding = 20.0;
}
