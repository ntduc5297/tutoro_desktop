class Assets {
  static const String appLogo = "assets/icons/ic_appicon.png";

  // splash screen assets
  static const String splashBackground = "assets/images/bg_screen_loading.png";
  static const String versionUpdateIcon =
      "assets/images/update_new_version_icon.png";

  // version update assets
  static const String logo = "assets/images/logo.png";

  // version update assets
  static const String logoApple = "assets/images/logo_apple.png";

  // login screen assets
  static const String circlePurple = "assets/images/circlePurple.png";

  static const String divideIcon = "assets/images/divide_icon.png";

  static const String vietnamSymbol = "assets/images/vietnam_symbol.png";

  static const String usSymbol = "assets/images/us_symbol.png";

  static const String microsoftIcon = "assets/images/microsoft_icon.svg";

  /*
  * Login though third system
  * */
  static const String tekyIcon = "assets/images/teky_icon.png";

  static const String fbIcon = "assets/images/fb_icon.svg";

  static const String googleIcon = "assets/images/google_icon.svg";

  static const String lockIcon = "assets/images/lock_icon.svg";

  static const String threeDotIcon = "assets/images/dot_icon.png";
  static const String otpIcon = "assets/images/otp_icon.png";

  /*
  * Otp Screen
  * */

  static const String forgotPass = "assets/images/forgot_password_image.svg";
  static const String newPassWhenForgot =
      "assets/images/new_pass_when_forgot.svg";

  static const String classIconActive = "assets/images/class_active.png";
  // Bottom navigation bar
  static const String classIconInactive = "assets/images/class_inactive.png";
  static const String scheduleIconActive = "assets/images/schedule_active.png";
  static const String scheduleIconInactive =
      "assets/images/schedule_inactive.png";
  static const String notificationIconActive =
      "assets/images/notification_active.png";
  static const String notificationIconInactive =
      "assets/images/notification_inactive.png";
  static const String chatIconActive = "assets/images/chat_active.png";
  static const String chatIconInactive = "assets/images/chat_inactive.png";
  static const String communityIconActive =
      "assets/images/community_active.png";
  static const String communityIconInactive =
      "assets/images/community_inactive.png";
  static const String moreIconActive = "assets/images/more_active.png";
  static const String moreIconInactive = "assets/images/more_inactive.png";
  static const String iconTeacherProfile =
      "assets/images/teacher_profile_icon.png";

  // Settings
  static const String iconLanguage = "assets/images/language_icon.png";
  static const String iconNotification = "assets/images/notification_icon.png";
  static const String iconPassword = "assets/images/password_icon.png";
  static const String iconLogOut = "assets/images/log_out_icon.png";
  static const String iconRightArrow = "assets/images/right_arrow_icon.png";
  static const String iconRoundedGreenCheck =
      "assets/images/rounded_green_check_icon.png";
  static const String greenCheckIcon = "assets/images/green_check_icon.png";
  // Languages setting
  static const String iconGreenCheckSvg = "assets/images/tick_green.svg";
  static const String passwordSettingIcon =
      "assets/images/password_setting_icon.png";

  /*
  * Password in settings
  * */

  static const String previewSecureIcon =
      "assets/images/preview_secure_icon.png";
  static const String notPreviewSecureIcon =
      "assets/images/not_preview_secure_icon.svg";
  static const String verifiedProfileIcon =
      "assets/images/verified_profile_badge_icon.png";

  /*
  * Teacher Profile
  * */
  static const String unverifiedProfileIcon =
      "assets/images/unverified_profile_badge_icon.png";
  static const String profile = "assets/images/profile_teacher.png";

  static const String jobJD = "assets/images/work_info_icon.png";
  static const String profileTeacher = "assets/images/personal_info_icon.png";
  static const String expJd = "assets/images/qualification_info_icon.png";
  static const String contractTeacher = "assets/images/contract_info_icon.png";
  static const String circleOrange = "assets/images/circle_orange.png";
  static const String circleRed = "assets/images/circle_red.png";
  static const String uploadImageIcon = "assets/images/image_upload_icon.svg";
  static const String cameraIcon = "assets/images/camera_icon.svg";
  static const String contractInfoHeaderDeco =
      "assets/images/contract_info_header_deco.svg";
  static const String academicLevelHeader =
      "assets/images/academic_level_header.svg";

  // Contract info
  static const String editIcon = "assets/images/edit_icon.png";

  // Qualification info
  static const String iconYellowClock = "assets/images/yellow_clock_icon.png";
  static const String classScheduleBg = "assets/images/class_schedule_bg.png";
  static const String downArrow = "assets/images/down_arrow.svg";
  static const String calendar = "assets/images/calendar.svg";
  static const String addIcon = "assets/images/add_icon.svg";

  // Class schedule info
  static const String calendarIcon = "assets/images/calendar_icon.png";
  static const String filterIcon = "assets/images/filter_icon.png";
  static const String paperIcon = "assets/images/paper_icon.png";
  static const String timeCircleIcon = "assets/images/time_circle_icon.png";
  static const String workIcon = "assets/images/work_icon.png";
  static const String locationIcon = "assets/images/location_icon.png";
  static const String chatIcon = "assets/images/chat_icon.png";
  static const String addUserIcon = "assets/images/add_user_icon.png";
  static const String xIcon = "assets/images/x_icon.png";
  static const String xNoPaddingIcon = "assets/images/no_padding_x_icon.png";
  static const String takeClassImg = "assets/images/take_class_img.png";
  static const String takeClassSuccessImg =
      "assets/images/take_class_success.png";
  static const String threeDotHorizontal =
      "assets/images/three_dot_horizontal.svg";

  // teaching manage
  static const String paperTransIcon =
      "assets/images/paper_transparent_icon.png";
  static const String paperGrayIcon = "assets/images/paper_gray_icon.png";

  // teaching classes
  static const String singleDotIcon = "assets/images/single_dot_icon.png";
  static const String timeClockIcon = "assets/images/time_clock_icon.png";
  static const String timeClockBlackIcon =
      "assets/images/time_clock_black_icon.svg";
  static const String threeDotsIcon = "assets/images/three_dots_icon.png";
  static const String threeUsersIcon = "assets/images/three_users_icon.png";
  static const String onGoingClassIcon = "assets/images/class_going_icon.png";
  static const String stoppedClassIcon = "assets/images/class_stopped.icon.png";
  static const String teachingClassIcon = "assets/images/teaching_class.svg";
  static const String squareTimeIcon = "assets/images/square_time_icon.svg";
  static const String questionIcon = "assets/images/question_icon.svg";
  static const String attentionLessonDetail =
      "assets/images/attention_lesson_detail.svg";
  static const String lessonPlan = "assets/images/lesson_plan.svg";
  static const String uploadImageLesson =
      "assets/images/upload_image_lesson.svg";
  static const String feedbackLesson = "assets/images/feedback_lesson.svg";
  static const String borrowMaterials = "assets/images/borrow_materials.svg";
  static const String chartLesson = "assets/images/chart_lesson.svg";
  static const String markingExercises = "assets/images/marking_exercises.svg";
  static const String resultLesson = "assets/images/result_lesson.svg";
  static const String lessonDetailHeader =
      "assets/images/lesson_detail_header.svg";
  static const String chatLessonDetail = "assets/images/chat_lesson_detail.svg";
  static const String leftArrowWhite = "assets/images/left_arrow_white.svg";
  static const String paperPlus = "assets/images/paper_plus.svg";

  // Syllabus
  static const String youtubeImage = "assets/images/youtube.svg";
  static const String pttImage = "assets/images/ptt.svg";
  static const String docImage = "assets/images/doc.svg";
  static const String popupBackground = "assets/images/popup_background.png";
  static const String arrowPopupBackground = "assets/images/arrow_white.png";

  // Periodic report
  static const String chartReportImage = "assets/images/chart_report.svg";
  static const String homeWorkDoneImage = "assets/images/hw_done.svg";
  static const String classStudyingImage = "assets/images/class_studying.svg";
  static const String lessonStudiedImage = "assets/images/lesson_studied.svg";
  static const String missionImage = "assets/images/mission.svg";

  // class detail
  static const String sortIcon = "assets/images/sort_icon.png";
  static const String purpleCheckIcon = "assets/images/purple_check_icon.png";
  static const String grayCheckIcon = "assets/images/gray_check_icon.png";

  // error dialog
  static const String errorDialogImg = "assets/images/error_popup_img.png";
  static const String rightArrowWithLineIcon =
      "assets/images/right_arrow_with_line_icon.png";
  static const String iconRoundedGrayCheck =
      "assets/images/rounded_gray_check_icon.png";
  static const String iconRoundedGrayCross =
      "assets/images/rounded_gray_cross_icon.png";
  static const String iconRoundedRedCross =
      "assets/images/rounded_red_cross_icon.png";
  static const String cancelClassSuccessImg =
      "assets/images/cancel_class_img.png";

  static const String filledStarIcon =
      "assets/images/rate_star_filled_icon.png";
  static const String emptyStarIcon = "assets/images/rate_star_empty_icon.png";
  static const String conversationIcon = "assets/images/conversation_icon.png";
  static const String birthdayGrayIcon = "assets/images/birthday_gray_icon.png";
  static const String singleUserGrayIcon = "assets/images/single_user_icon.png";
  static const String calendarGrayIcon = "assets/images/calendar_gray_icon.png";

  static const String readingIcon = "assets/images/reading_icon.png";
  static const String microphoneIcon = "assets/images/microphone_icon.png";
  static const String fastTimeIcon = "assets/images/fast_time_icon.png";
  static const String standingBoardIcon =
      "assets/images/standing_board_icon.png";
  static const String searchIcon = "assets/images/search_icon.png";
  static const String cloudUploadIcon = "assets/images/cloud_upload_icon.png";
  static const String darkXCircleIcon = "assets/images/dark_x_circle_icon.png";
  static const String playRoundedIcon = "assets/images/play_circle_icon.png";

  static const String productLesson = "assets/images/product_lesson.png";

  static const String uploadIcon = "assets/images/upload_image.svg";

  static const String threePeopleIcon = "assets/images/three_people_icon.png";
  static const String twoPeopleIcon = "assets/images/two_people_icon.png";
  static const String notebook1Icon = "assets/images/notebook_1_icon.png";
  static const String notebook2Icon = "assets/images/notebook_2_icon.png";
  static const String ribbonBadgeIcon = "assets/images/ribbon_badge_icon.png";
  static const String folderStarIcon = "assets/images/folder_star_icon.png";
  static const String videoCollectionIcon =
      "assets/images/video_collection_icon.png";

  static const String checkAllIcon = "assets/images/check_all_icon.png";
  static const String trashDeleteIcon = "assets/images/trash_delete_icon.png";
  static const String blueChatIcon = "assets/images/blue_chat_icon.png";
  static const String turkishRoseCalendarIcon =
      "assets/images/turkish_rose_calendar_icon.png";
  static const String greenBillIcon = "assets/images/green_bill_icon.png";

  static const String moreNotificationsIcon =
      "assets/images/more_notification_icon.png";
  static const String morePayslipIcon = "assets/images/more_payslip_icon.png";
  static const String moreReferIcon = "assets/images/more_refer_icon.png";
  static const String moreSettingsIcon = "assets/images/more_settings_icon.png";
  static const String moreCoursesIcon = "assets/images/more_courses_icon.png";
  static const String moreReportsIcon = "assets/images/more_paper_icon.png";
  static const String moreHelpIcon = "assets/images/more_help_icon.png";
  static const String redHeartIcon = "assets/images/red_heart_icon.png";
  static const String timeRedCircleIcon =
      "assets/images/time_red_circle_icon.png";

  static const String referAndEarnImg = "assets/images/refer_and_earn_img.png";
  static const String referAndEarnSvgBg = "assets/images/refer_and_earn_bg.svg";
  static const String referAndEarnBg = "assets/images/refer_and_earn_bg.png";

  static const String payslipHeaderDeco =
      "assets/images/payslip_header_deco.png";
  static const String payslipHeaderDecoSvg =
      "assets/images/payslip_header_deco.svg";
  static const String infoIcon = "assets/images/info_icon.png";

  static const String createPassImg = "assets/images/create_pass_img.png";

  static const String iconSegmentChatActive =
      "assets/images/icon_segment_chat_active.png";
  static const String iconSegmentChatInactive =
      "assets/images/icon_segment_chat_inactive.png";
  static const String iconSegmentCallActive =
      "assets/images/icon_segment_call_active.png";
  static const String iconSegmentCallInactive =
      "assets/images/icon_segment_call_inactive.png";
  static const String iconNewConversation =
      "assets/images/icon_new_conversation.png";
  static const String iconIncomingCall = "assets/images/icon_incoming_call.png";
  static const String iconOutgoingCall = "assets/images/icon_outgoing_call.png";
  static const String iconMissedCall = "assets/images/icon_missed_call.png";
  static const String iconSend = "assets/images/icon_send.png";

  static const String iconReferAnEarnShare =
      "assets/images/refer_earn_share.svg";
  static const String iconReferAnEarnJoin = "assets/images/refer_earn_join.svg";
  static const String iconPhoneRefer = "assets/images/refer_phone_icon.svg";
  static const String iconEmailRefer = "assets/images/refer_email_icon.svg";
  static const String phoneReferAction = "assets/images/phone_refer_action.png";
  static const String linkReferAction = "assets/images/link_refer_action.png";

  static const String iconSentMessage = "assets/images/icon_sent_message.png";
  static const String iconEmotion = "assets/images/icon_emotion.png";
  // Qr
  static const String iconQr = "assets/images/qrcode_icon.svg";
  static const String imageQrConfirm = "assets/images/qr_confirm_image.png";
  static const String descriptionImg = "assets/images/description_img.png";
  static const String backgroundLoginImg = "assets/images/bg_why.jpg";

  Assets._();
}
