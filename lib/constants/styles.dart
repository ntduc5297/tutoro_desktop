import '/constants/font_family.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaling/flutter_screen_scaling.dart';
import 'colors.dart';

// ignore: avoid_classes_with_only_static_members
class Styles {
  static TextStyle commonTitleStyles = TextStyle(
      fontFamily: FontFamily.quickSand,
      fontWeight: FontWeight.w700,
      fontSize: ScreenScale.convertFontSize(18),
      color: AppColors.mainTextColor);

  static const transparentBorder =
      OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
}
