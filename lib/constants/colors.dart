import 'package:flutter/material.dart';

class AppColors {
  AppColors._(); // this basically makes it so you can't instantiate this class

  static const Map<int, Color> purple = <int, Color>{
    50: Color(0xFFD0C3FD),
    100: Color(0xFFC0AFFD),
    200: Color(0xFFB19CFC),
    300: Color(0xFFA188FC),
    400: Color(0xFF9174FB),
    500: Color(0xFF7B57FB),
    600: Color(0xFF724CFA),
    700: Color(0xFF6238FA),
    800: Color(0xFF5224F9),
    900: Color(0xFF4310F9),
  };

  static const Color mainBgColor = Color(0xFFFBFDFF);
  static const Color mainColor = Color(0xFF7B57FB);
  static const Color mainTextColor = Color(0xFF363E59);
  static const Color secondTextColor = Color(0xFF4B5574);
  static const Color thirdTextColor = Color(0xFF7788A3);
  static const int mainColorHex = 0xFF7B57FB;
  static const Color secondBorderColor = Color(0xFFECF7FB);
  static const Color mainShadowColor = Color(0xffcdd8e3);
  static const Color mainBorderColor = Color(0xfff8f8f8);
  static const Color thirdBorderColor = Color(0xff7f88a3);
  static const Color orange = Color(0xfff2994a);

  static const Color greenPositiveBgColor = Color(0xffddf0dd);
  static const Color greenPositiveTextColor = Color(0xff27ae60);
  static const Color yellowNegativeBgColor = Color(0xffFFFBEA);
  static const Color yellowNegativeTextColor = Color(0xfff2c94c);
  static const Color redNegativeBgColor = Color(0xffffe4e0);
  static const Color redNegativeTextColor = Color(0xffd35d52);
}
