// ignore_for_file: constant_identifier_names
class Constants {
  Constants._();

  static const int RequiredPasswordLength = 8;

  static const int PhoneNumberMaxLength = 9;

  static const String MicrosoftClientId =
      "ea293f06-ab82-401e-b303-389411c78e56";

  static const String reCaptchaSecretKey =
      "6Lcs1SsbAAAAAHESl90_fCAxcG4hFSY4B8uWrkky";
}

enum CancelClassActionType { submitCancellationForm, deleteCancellationForm }

enum ReportType { lessonReport, classReport }
