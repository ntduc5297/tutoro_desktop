class FontFamily {
  FontFamily._();

  static String productSans = "ProductSans";
  static String roboto = "Roboto";
  static String quickSand = "Quicksand";
  static String quickSandSemibold = "Quicksand";
}
