import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

/// My Logger class
class MyLogger {
  final Logger _logger = Logger(
      filter: DebugLogOnlyFilter(), printer: PrettyPrinter(), output: null);

  static final MyLogger _singleton = MyLogger._internal();

  /// Singleton getter
  factory MyLogger() {
    return _singleton;
  }

  MyLogger._internal();

  /// Get Logger instance
  static Logger getLoggerInstance() {
    return MyLogger()._logger;
  }
}

/// Log filter only for debug mode
class DebugLogOnlyFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return kDebugMode;
  }
}
