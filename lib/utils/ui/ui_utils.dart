import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screen_scaling/flutter_screen_scaling.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../locale/app_localization.dart';

class UIUtils {
  static AppBar buildCommonAppBar(BuildContext context, String title,
      {TextStyle? titleStyle,
      bool backBtnVisible = true,
      Color backgroundColor = Colors.transparent,
      Widget? rightAction}) {
    final defaultTitleStyle = TextStyle(
        fontSize: ScreenScale.convertFontSize(18), fontWeight: FontWeight.bold);

    return AppBar(
      backgroundColor: backgroundColor,
      leading: backBtnVisible
          ? IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            )
          : Container(),
      title: Text(
        title,
        style: titleStyle ?? defaultTitleStyle,
      ),
      centerTitle: true,
      shadowColor: Colors.transparent,
      actions: rightAction != null ? [rightAction] : [],
    );
  }

  static void showErrorMessage(String? message, BuildContext context) {
    Future.delayed(Duration(milliseconds: 0), () {
      if (message != null && message.isNotEmpty) {
        FlushbarHelper.createError(
          message: message,
          title: AppLocalizations.of(context).translate('common_error'),
          duration: Duration(seconds: 3),
        ).show(context);
      }
    });
  }

  static void showSuccessMessage(String? message, BuildContext context) {
    Future.delayed(Duration(milliseconds: 0), () {
      if (message != null && message.isNotEmpty) {
        FlushbarHelper.createSuccess(
          message: message,
          title: AppLocalizations.of(context).translate('common_success'),
          duration: Duration(seconds: 2),
        ).show(context);
      }
    });
  }
}
