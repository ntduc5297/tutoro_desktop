import 'package:flutter/material.dart';
import 'package:tutorO/ui/forgot_password/forgot_password.dart';
import 'package:tutorO/ui/home/home.dart';
import 'package:tutorO/ui/login/login.dart';
import 'package:tutorO/ui/splash/splash.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String login = '/login';
  static const String home = '/home';
  static const String forgotpassword = '/forgotpassword';

  static final routes = <String, WidgetBuilder>{
    splash: (context) => SplashScreen(),
    login: (context) => LoginScreen(),
    home: (context) => HomeScreen(),
    forgotpassword: (context) => ForgotPasswordScreen(),
  };
}
