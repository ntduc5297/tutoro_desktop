import '../../main.dart';
import '../module/local_module.dart';
import '../module/network_module.dart';
import '../module/preference_module.dart';
import '/data/repository/repository.dart';
import 'app_component.inject.dart' as g;

/// The top level injector that stitches together multiple app features into
/// a complete app.
abstract class AppComponent {
  MyStatefulApp get app;

  static Future<AppComponent> create(
    NetworkModule networkModule,
    LocalModule localModules,
    PreferenceModule preferenceModule,
  ) async {
    return await g.AppComponent$Injector.create(
      networkModule,
      localModules,
      preferenceModule,
    );
  }

  /// An accessor to RestClient object that an application may use.
  Repository getRepository();

  // AuthenticationRepository getAuthenticationRepository();

  // ProfileRepository getProfileRepository();

  // MasterDataRepository getMasterDataRepository();

  // ContractRepository getContractRepository();

  // AcademicLevelRepository getAcademicLevelRepository();

  // CertificationRepository getCertificationRepository();

  // WorkExpRepository getWorkExpRepository();

  // ClassRepository getClassRepository();

  // ClassEvaluationRepository getClassEvaluationRepository();

  // NotificationRepository getNotificationRepository();
}
