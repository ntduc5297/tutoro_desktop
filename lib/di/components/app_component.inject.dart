import 'package:dio/dio.dart';
import '../../data/repository/repository.dart';
import '../../data/network/dio_client.dart';
import '../../data/sharedpref/shared_preference_helper.dart';
import '../../main.dart';
import '../module/local_module.dart';
import '../module/network_module.dart';
import '../module/preference_module.dart';
import 'app_component.dart';

class AppComponent$Injector implements AppComponent {
  final LocalModule _localModule;

  final PreferenceModule _preferenceModule;

  SharedPreferenceHelper? _singletonSharedPreferenceHelper;
  Repository? _singletonRepository;

  Dio? _singletonDio;

  DioClient? _singletonDioClient;

  AppComponent$Injector._(this._localModule, this._preferenceModule);
  MyStatefulApp _createMyStatefulApp() => MyStatefulApp();
  @override
  MyStatefulApp get app => _createMyStatefulApp();

  Dio _createDio() => _singletonDio ??=
      _localModule.provideDio(_createSharedPreferenceHelper());
  DioClient _createDioClient() =>
      _singletonDioClient ??= _localModule.provideDioClient(_createDio());
  SharedPreferenceHelper _createSharedPreferenceHelper() =>
      _singletonSharedPreferenceHelper ??=
          _preferenceModule.provideSharedPreferenceHelper();
  static Future<AppComponent> create(NetworkModule _, LocalModule localModule,
      PreferenceModule preferenceModule) async {
    final injector = AppComponent$Injector._(localModule, preferenceModule);

    return injector;
  }

  @override
  Repository getRepository() {
    // TODO: implement getRepository
    throw UnimplementedError();
  }
}
