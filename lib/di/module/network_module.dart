import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../data/network/dio_client.dart';
import '../../main.dart';

import '../../ui/error_dialog/error_dialog.dart';
import '../../utils/my_logger/my_logger.dart';
import '../../models/common_response/common_response.dart';
import '../../data/network/apis/posts/post_api.dart';
import '/data/network/constants/endpoints.dart';
import '/utils/ui/ui_utils.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:flutter/src/material/dialog.dart';
import '../../data/sharedpref/shared_preference_helper.dart';
import '../../data/sharedpref/constants/preferences.dart';
import '../../di/module/preference_module.dart';

class NetworkModule extends PreferenceModule {
  // ignore: non_constant_identifier_names
  final String TAG = "NetworkModule";
  bool isShowingModal = false;
  late StackTrace stackTrace;
  // DI Providers:--------------------------------------------------------------
  /// A singleton dio provider.
  ///
  /// Calling it multiple times will return the same instance.

  Dio provideDio(SharedPreferenceHelper sharedPrefHelper) {
    final dio = Dio();
    dio
      ..options.baseUrl = Endpoints.baseUrl
      ..options.connectTimeout = Endpoints.connectionTimeout
      ..options.receiveTimeout = Endpoints.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ));
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      var prefs = await SharedPreferences.getInstance();
      final token = prefs.getString(Preferences.auth_token);
      MyLogger.getLoggerInstance().d('Auth token: $token');
      if (token != null) {
        options.headers.putIfAbsent('Authorization', () => 'Bearer $token');
      } else {
        print('Auth token is null');
      }

      return handler.next(options); //continue
      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onResponse: (response, handler) {
      // Do something with response data
      dynamic data = response.data;
      _handleServerResError(data);
      return handler.next(response); // continue
      // If you want to reject the request with a error message,
      // you can reject a `DioError` object eg: return `dio.reject(dioError)`
    }, onError: (e, handler) {
      print("Receive DioError: $e");
      if (e is DioError) {
        if (e.response != null) {
          dynamic data = e.response!.data;
          _handleServerResError(data);

          return handler.resolve(e.response!);
        }
        // Do something with response error
        return handler.next(e); //continue
      }

      // If you want to resolve the request with some custom data，
      // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    }));

    return dio;
  }

  /// A singleton dio_client provider.
  ///
  /// Calling it multiple times will return the same instance.

  DioClient provideDioClient(Dio dio) => DioClient(dio);

  // Api Providers:-------------------------------------------------------------
  // Define all your api providers here
  /// A singleton post_api provider.
  ///
  /// Calling it multiple times will return the same instance.

  // PostApi providePostApi(DioClient dioClient) => PostApi(dioClient);
// Api Providers End:---------------------------------------------------------

  void _handleServerResError(Map<String, dynamic> data) async {
    Map<String, dynamic> statusResJson = data["message"];
    final parsedResponse = CommonResponse.fromJson(json: statusResJson);
    if (400 <= parsedResponse.statusCode && parsedResponse.statusCode <= 499) {
      Map<String, dynamic> errResJSON = data["error"];
      if (errResJSON.isEmpty) {
        UIUtils.showErrorMessage(
            parsedResponse.message, globalNavigatorKey.currentContext!);
      }
    } else if (parsedResponse.statusCode == 500) {
      globalNavigatorKey.currentContext!.loaderOverlay.hide();
      if (isShowingModal == false) {
        isShowingModal = true;
        showDialog(
            context: globalNavigatorKey.currentContext!,
            builder: (dialogContext) {
              return ErrorDialog(
                dialogContext: dialogContext,
                message: parsedResponse.message,
                btnConfirmCallback: () {
                  isShowingModal = false;
                  // Navigator.of(context).pushNamed(Routes.takeClassReqSent);
                },
              );
            });
      }
    }
  }
}
