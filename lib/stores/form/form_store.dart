import '/stores/error/error_store.dart';
import 'package:mobx/mobx.dart';
import 'package:validators/validators.dart';
// ignore: import_of_legacy_library_into_null_safe

part 'form_store.g.dart';

// ignore: library_private_types_in_public_api
class FormStore = _FormStore with _$FormStore;

abstract class _FormStore with Store {
  final FormErrorState error = FormErrorState();
  final FormErrorState checkNull = FormErrorState();
  @observable
  String name = '';

  @observable
  String email = '';

  @observable
  String password = '';

  @observable
  String number = '';

  @observable
  ObservableFuture<bool> usernameCheck = ObservableFuture.value(true);

  @computed
  bool get isUserCheckPending => usernameCheck.status == FutureStatus.pending;

  @computed
  bool get canLogin => checkNull.hasErrors;

  late List<ReactionDisposer> _disposers;

  @action
  void setupValidations() {
    _disposers = [
      reaction((_) => name, validateUsername),
      reaction((_) => email, validateEmail),
      reaction((_) => password, validatePassword),
      reaction((_) => number, validateNumber)
    ];
  }

  @action
  setEmail(String value) {
    email = value;

    validateAll();
  }

  @action
  setNumber(String value) {
    number = value;
    validateAll();
  }

  @action
  setPassword(String value) {
    password = value;
    validateAll();
  }

  @action
  // ignore: avoid_void_async
  Future validateUsername(String value) async {
    if (isNull(value) || value.isEmpty) {
      error.username = 'Không được để trống';
      checkNull.username = null;
      return;
    }

    try {
      usernameCheck = ObservableFuture(checkValidUsername(value));

      error.username = null;

      final isValid = await usernameCheck;
      if (!isValid) {
        error.username = 'Tài khoản không được là "admin"';
        return;
      }
    } on Object catch (_) {
      error.username = null;
    }

    error.username = null;
  }

  @action
  void validatePassword(String value) {
    if (isNull(value) || value.isEmpty) {
      error.password = '';
      checkNull.password = null;
      return;
    }
    if (value.length < 4) {
      error.password = 'Mật khẩu quá ngắn';
      checkNull.password = null;
      return;
    } else {
      error.password = '';
      checkNull.password = '';
      return;
    }
  }

  @action
  void validateNumber(String value) {
    // print("111111   ${value}");
    if (isNull(value) || value.isEmpty) {
      error.number = '';
      checkNull.number = null;
      return;
    }
    error.number = isNumeric(value) ? null : 'Số điện thoại không hợp lệ';
    checkNull.number = isNumeric(value) ? null : '';

    if (value.length > 10 || value.length < 9) {
      error.number = ' Số điện thoại không hợp lệ';
      checkNull.number = '';
    } else {
      checkNull.number = null;
    }
  }

  @action
  void validateEmail(String value) {
    if (isNull(value) || value.isEmpty) {
      error.email = 'Không được để trống';
      return;
    }
    error.email = isEmail(value) ? null : 'Không phải là email';
  }

  Future<bool> checkValidUsername(String value) async {
    await Future.delayed(const Duration(seconds: 1));

    return value != 'admin';
  }

  void dispose() {
    for (final d in _disposers) {
      d();
    }
  }

  void validateAll() {
    validatePassword(password);
    validateEmail(email);
    validateUsername(name);
    validateNumber(number);
  }
}

// ignore: library_private_types_in_public_api
class FormErrorState = _FormErrorState with _$FormErrorState;

abstract class _FormErrorState with Store {
  @observable
  String? username;

  @observable
  String? email;

  @observable
  String? password;
  @observable
  String? number;

  @computed
  bool get hasErrors => password != null && number == null;
}
